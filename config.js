const config = {
    type: Phaser.AUTO,
    width: 960,
    height: 720,
    backgroundColor: "000000",
    parent: 'phaser-game',
    scene: [Manager, GameScene, ExploreScene, PlayScene, CreateScene]
};

const app = new Phaser.Game(config);