
import os

# Set the directory containing the .js files
directory = r'c:/wamp64/www/GAMENAME/'
game_directory = directory + 'Game/'
go_directory = directory + 'Game/GameObjects/'
scenes_directory = directory + 'Game/Scenes/'

# Set the name of the output file
output_file = "all.js"

# Get a list of all .js files in the directory
go_js_files = [f for f in os.listdir(go_directory) if f.endswith('.js')]
scenes_js_files = [f for f in os.listdir(scenes_directory) if f.endswith('.js')]
game_js_files = [f for f in os.listdir(game_directory) if f.endswith('.js')]

# Open the output file for writing
with open(output_file, 'w') as outfile:
    # Loop through each .js file and write its contents to the output file
    for js_file in game_js_files:
        with open(os.path.join(game_directory, js_file), 'r') as infile:
            outfile.write(infile.read())
    for js_file in go_js_files:
        with open(os.path.join(go_directory, js_file), 'r') as infile:
            outfile.write(infile.read())
    for js_file in scenes_js_files:
        with open(os.path.join(scenes_directory, js_file), 'r') as infile:
            outfile.write(infile.read())        

    config_file = os.path.join(directory, "config.js")
    with open(config_file, 'r') as config:
            outfile.write(config.read())

# Add a line at the end of all.js
with open(output_file, 'a') as outfile:
    outfile.write("\nconst global = {};\n")
    outfile.write("global.canvas = Canvas.getInstance();\n")
    outfile.write("global.Log = Log;\n")
    outfile.write("global.util = Util.getInstance();\n")
    outfile.write("global.resources = Resources;\n")
    outfile.write("global.statistics = Statistics;\n")
    outfile.write("global.Gamemode = Gamemode;\n")
    outfile.write("global.listenerManager = ListenerManager.getInstance();\n")

# Print a message to the console
print("All .js files in " + directory + " have been merged into " + output_file)    
