class GameScene extends Phaser.Scene {

    constructor(key) {
        super(key);
    }

    preload(){

    }

    create(){

    }

    update(){

    }

    addStats = (mode) => {
        try {
            global.statistics.addStats(mode);
        } catch (error) {
            console.log(error);
        }
    }

    updateStats = (score) => {
        try {
            global.statistics.updateStats(score);
        } catch (error) {
            console.log(error);
        }
    }

}