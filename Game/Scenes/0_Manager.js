// Description: This file is responsible for loading the game mode and the scenario.

class Manager extends Phaser.Scene {

    constructor() {
        super({key: 'Manager', active: true});

        try {
            global.util.callOnGamemodeChange(this.loadGameMode);
        } catch (error) {
            console.log("No game mode is selected." + error);

        }
      }
    
    preload() {

        try {
            this.loadGameMode();
        } catch (error) {
            console.log("Loading select scene because global is not defined " + error);
            this.scene.start('PlayScene');
        }

    }

    create() {
        console.log("Manager scene created.");
    }

    update() {
        
    }

    loadScenario = (scenario) => {


    }

    loadGameMode = () => {
        let gameMode = global.util.getGamemode();
        // Stop all scenes except the manager
        this.scene.manager.scenes.forEach(scene => {
            if(scene.scene.key !== 'Manager')
            {
                scene.scene.stop();
            }
        });

        switch(gameMode) {
            case global.Gamemode.Evaluate:
                this.fetchExercice();
                this.showTutorialInput(false);
                this.scene.start('PlayScene');
                break;

            case global.Gamemode.Train:
                this.fetchExercice();
                this.showTutorialInput(false);
                this.scene.start('PlayScene');
                break;

            case global.Gamemode.Explore:
                this.showTutorialInput(false);
                this.scene.start('ExploreScene');
                break;
                
            case global.Gamemode.Create:
                this.showTutorialInput(true);
                this.scene.start('CreateScene');
                break;
        }
    }

    fetchExercice = () => {
        try {
            let exercise = global.resources.getExercice();
            if(exercise != null) {
                exercise = JSON.parse(exercise.exercice);
                this.loadScenario(exercise);
            } else {
                console.log("No exercise exists. Loading default scenario.");
                this.loadScenario(scenario01);
            }
        } catch (error) {
            console.log("Error in fetchExercice: " + error);
        }

    }

    showTutorialInput = (flag) => { 
        try {
            if(flag) {
                global.util.showTutorialInputs('btnUpload', 'upload_file');
            } else {
                global.util.hideTutorialInputs('btnUpload', 'upload_file');
            }
        } catch (error) {
            console.log("Error in showTutorialInput: " + error);
        }
    }

}